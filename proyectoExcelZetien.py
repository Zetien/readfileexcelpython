import openpyxl
import re
import pprint

def checkEmail(email):
    regex = regex = r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])"
    return re.match(regex, email) is not None if email is not None else False

def valid_number(number):
        return  isinstance(number,int)

def valid_float(number):
        return isinstance(number, float)



class ReadFile:

    def __init__(self, path):
        self.path = path
        self.valid_data=[]
        self.invalid_data=[]
        self.main_data_for_ship = {'clase1': [], 'clase2': [], 'clase3': [],'clase4': []}



    def read_file(self):
        wb_obj = openpyxl.load_workbook(self.path)
        sheet_obj = wb_obj.active
        first_row = []

        for col in range(sheet_obj.max_column):
            first_row.append(sheet_obj.cell(1, col + 1).value)
        data = []

        for row in range(2, sheet_obj.max_row + 1):
            elm = {}
            for col in range(sheet_obj.max_column):
                elm[first_row[col]] = sheet_obj.cell(row, col + 1).value
            data.append(elm)
        return data

    def validar_clase(self,items):
        if items.get('ship_mode') == 'First Class':
            self.main_data_for_ship.get('clase1').append(items)
        elif items.get('ship_mode') == 'Second Class':
            self.main_data_for_ship.get('clase2').append(items)
        elif items.get('ship_mode') == 'Standard Class':
            self.main_data_for_ship.get('clase3').append(items)
        elif items.get('ship_mode') == 'Same Day':
            self.main_data_for_ship.get('clase4').append(items)

    def clean_data(self, data):
        contador=1
        info_filas_con_problemas={'Fila a corregir':[]}
        for items in data:
            contador+=1
            self.validar_clase(items)
            if not valid_number(items.get('row_id')):
                self.invalid_data.append(items)
                info_filas_con_problemas['Fila a corregir'].append(contador)

            elif not checkEmail(items.get('email')):
                self.invalid_data.append(items)
                info_filas_con_problemas['Fila a corregir'].append(contador)
            elif not valid_float(items.get('sales')) or not items.get('sales'):
                self.invalid_data.append(items)
                info_filas_con_problemas['Fila a corregir'].append(contador)
            else:
                self.valid_data.append(items)
        #pprint.pprint(len(self.invalid_data))
        #pprint.pprint(len(self.valid_data))
        return self.invalid_data, self.valid_data, self.main_data_for_ship, info_filas_con_problemas



def main():
    test = ReadFile('./Sample - Superstore.xlsx')
    #pprint.pprint(test.clean_data(test.read_file()))
    trash_data, cleaned_data, data_ship, filasConProblemas=test.clean_data(test.read_file())
    mensaje = """
       $$$$$$$\                                $$\   $$\                     $$\                     
       $$  __$$\                               $$ |  $$ |                    $$ |                    
       $$ |  $$ | $$$$$$\   $$$$$$$\ $$\   $$\ $$ |$$$$$$\    $$$$$$\   $$$$$$$ | $$$$$$\   $$$$$$$\ 
       $$$$$$$  |$$  __$$\ $$  _____|$$ |  $$ |$$ |\_$$  _|   \____$$\ $$  __$$ |$$  __$$\ $$  _____|
       $$  __$$< $$$$$$$$ |\$$$$$$\  $$ |  $$ |$$ |  $$ |     $$$$$$$ |$$ /  $$ |$$ /  $$ |\$$$$$$\  
       $$ |  $$ |$$   ____| \____$$\ $$ |  $$ |$$ |  $$ |$$\ $$  __$$ |$$ |  $$ |$$ |  $$ | \____$$\ 
       $$ |  $$ |\$$$$$$$\ $$$$$$$  |\$$$$$$  |$$ |  \$$$$  |\$$$$$$$ |\$$$$$$$ |\$$$$$$  |$$$$$$$  |
       \__|  \__| \_______|\_______/  \______/ \__|   \____/  \_______| \_______| \______/ \_______/

       """
    print(mensaje)
    print('Error Demaciados datos para mostrar en lista los datos... solo se mostrara los datos estadisticos:')
    print('cantidad de Datos Correctos en el archivo: ',(len(cleaned_data)))
    print('cantidad de datos incorrectos en el archivo', (len(trash_data)))
    print('cantidad de elementos en First Class: ',(len(data_ship.get('clase1'))))
    print('cantidad de elementos en Second Class: ',(len(data_ship.get('clase2'))))
    print('cantidad de elementos en Standart Class: ',(len(data_ship.get('clase3'))))
    print('cantidad de elementos en Same Day: ',(len(data_ship.get('clase4'))))

    pprint.pprint(filasConProblemas)




if __name__ == '__main__':
    print('Nota: el archivo a leer es un poco grande por favor tener paciencia mientras muestra los resultados...')
    main()

